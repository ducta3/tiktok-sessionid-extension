function getCookies(domain, name, callback) {
    try {
        chrome.cookies.get({ "url": domain, "name": name }, function (cookie) {
            if (cookie && callback) {
                callback(cookie.value);
            } else {
                callback('');
            }
        });
    } catch (e) {
        alert(e);
    }
}

let accessToken = '';

getCookies("https://app.gostream.co", 'user_token', function (userToken) {
    if (!userToken) {
        alert("Please login on GoStream");
        chrome.tabs.create({ url: 'https://app.gostream.co' });
    }

    accessToken = userToken;
})

document.getElementById('getRTMP').addEventListener('click', async () => {
    document.getElementById("loading_data").classList.add("show");
    let response;
    getCookies("https://www.tiktok.com", "sessionid", function (sessionid) {
        if (!sessionid) {
            alert("Please login on TikTok");
            chrome.tabs.create({ url: 'https://www.tiktok.com' });
        }

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    try {
                        response = JSON.parse(xhttp.responseText);
                        let rtmpUrl = response.rtmp ? response.rtmp : '';
                        if (!rtmpUrl) {
                            throw new Error("Couldn't get rtmp url");
                        }
                        // let streamKey = rtmpUrl.split('/').pop();
                        // let serverUrl = rtmpUrl.replace(streamKey, '');
                        // document.getElementById("server_url").value = serverUrl;
                        // document.getElementById("stream_key").value = streamKey;
                        document.getElementById("server_url").value = rtmpUrl;
                        document.getElementById("loading_data").classList.remove("show");
                        return;
                    } catch (e) {
                        console.log(e);
                    }
                }
                if (this.status == 500) {
                    response = JSON.parse(xhttp.responseText);
                    if (response.code == 'auth/id-token-expired') {
                        alert("Please login on GoStream");
                        chrome.tabs.create({ url: 'https://app.gostream.co' });
                        return;
                    }
                }

                alert("Can't get RTMP URL");
                document.getElementById("loading_data").classList.remove("show");
            }
        };
        xhttp.open("GET", `https://app.gostream.co/api/tiktok/rtmp?session_id=${sessionid}`, true);
        xhttp.setRequestHeader("Authorization", `Bearer ${accessToken}`);
        xhttp.send();
    });
});

function copyTextToClipboard(str, options) {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    // Alert the copied text
    // alert("Text copied");
}
document.getElementById('btn-copy-1').addEventListener('click', async () => {
    var textInput = document.getElementById("server_url");
    if (!textInput.value) return;

    copyTextToClipboard(textInput.value);

    let btnCopy = document.getElementById('btn-copy-1');

    btnCopy.innerText = "Copied";
    let timeoutCopy = setTimeout(() => {
        btnCopy.innerText = "Copy";
        clearTimeout(timeoutCopy);
    }, 2000);
});

// document.getElementById('btn-copy-2').addEventListener('click', async () => {
//     var textInput = document.getElementById("stream_key");
//     if (!textInput.value) return;

//     copyTextToClipboard(textInput.value);
// });

